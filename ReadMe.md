# Kitalulus Technical Test - Rest API Server

Hi, this is the result of a technical test that I did.\
\
**Tech stack that I used:**\
    1. Go as a programming language\
    2. Fiber as a framework for go\
    3. MySQL as a database service\
    4. Docker for Containerization program\
    5. Docker Compose for defining and running multi-container Docker\
\
To make it easier to run this program, I have created a docker-compose file\
inside docker compose file i have defined for 2 container\
    1. Container for database (MySQL)\
    2. Container for rest api server (this program)\
\
I also include the postman export collection file in this repository
\
this server can be run inside project folder by typing

``` bash
    docker-compose -f docker-compose.yaml build
    docker-compose -f docker-compose.yaml up -d
```

\
after the container is successfully created the server rest api will go down for a few seconds because the restapi server has to reconnect to the mysql server, because the mysql server also runs on top of the docker container
