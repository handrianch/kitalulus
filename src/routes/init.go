package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/handrixn/kitalulus-test/src/handlers"
)

func InitRouter(app *fiber.App) {
	api := app.Group("/api")

	qh := handlers.QuestionHandlers{}
	questions := api.Group("/questions")
	questions.Get("/", qh.GetList)
	questions.Get("/:uuid", qh.GetDetail)
	questions.Post("/", qh.Create)
	questions.Put("/:uuid", qh.Update)
	questions.Delete("/:uuid", qh.Delete)
}
