package transformers

import (
	"time"

	"github.com/google/uuid"
	"github.com/handrixn/kitalulus-test/src/models"
)

type QuestionTransformer struct {
	ID        uuid.UUID `json:"id"`
	Question  string    `json:"question"`
	IsActive  bool      `json:"isActive"`
	CreatedBy string    `json:"createdBy"`
	UpdatedBy string    `json:"updatedBy"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

func (qt *QuestionTransformer) List(i interface{}) []QuestionTransformer {
	m := i.([]models.Question)

	var response []QuestionTransformer

	for _, item := range m {
		response = append(response, QuestionTransformer{
			ID:        item.UUID,
			Question:  item.Question,
			IsActive:  item.IsActive,
			CreatedBy: item.CreatedBy,
			UpdatedBy: item.UpdatedBy,
			CreatedAt: item.CreatedAt,
			UpdatedAt: item.UpdatedAt,
		})
	}

	return response
}

func (qt *QuestionTransformer) Detail(i interface{}) QuestionTransformer {
	m := i.(*models.Question)

	return QuestionTransformer{
		ID:        m.UUID,
		Question:  m.Question,
		IsActive:  m.IsActive,
		CreatedBy: m.CreatedBy,
		UpdatedBy: m.UpdatedBy,
		CreatedAt: m.CreatedAt,
		UpdatedAt: m.UpdatedAt,
	}
}
