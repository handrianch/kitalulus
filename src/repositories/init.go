package repositories

import (
	"gorm.io/gorm"
)

var DB *gorm.DB

func InitRepositories(db *gorm.DB) {
	DB = db
}
