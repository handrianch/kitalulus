package repositories

import (
	"github.com/gofiber/fiber/v2"
	"github.com/handrixn/kitalulus-test/src/models"
)

type QuestionRepository struct{}

func (qr *QuestionRepository) FindAll(where fiber.Map, offset, limit int) ([]models.Question, int64, error) {
	var m []models.Question

	results := DB
	countQuery := DB.Model(&models.Question{})

	if where != nil {
		results = results.Where(where)
		countQuery = countQuery.Where(where)
	}

	var count int64
	countQuery.Count(&count)

	results = results.Offset(offset).Limit(limit).Find(&m)

	if results.Error != nil {
		return nil, 0, results.Error
	}

	return m, count, nil
}

func (qr *QuestionRepository) FindOne(uuid string) (*models.Question, error) {
	var m models.Question

	results := DB.Where("uuid = ?", uuid).First(&m)

	if results.Error != nil {
		return nil, results.Error
	}

	return &m, nil
}

func (qr *QuestionRepository) Create(model *models.Question) (*models.Question, error) {
	results := DB.Create(model)

	if results.Error != nil {
		return nil, results.Error
	}

	return model, nil
}

func (qr *QuestionRepository) Delete(uuid string) error {
	var m models.Question

	results := DB.Where("uuid = ?", uuid).Delete(&m)

	if results.Error != nil {
		return results.Error
	}

	return nil
}

func (qr *QuestionRepository) Update(uuid string, model *models.Question) (*models.Question, error) {
	results := DB.Where("uuid = ?", uuid).Updates(model)

	if results.Error != nil {
		return nil, results.Error
	}

	updatedData, err := qr.FindOne(uuid)

	if err != nil {
		return nil, results.Error
	}

	return updatedData, nil
}
