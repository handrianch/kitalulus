package language

type Language struct {
	UUIDNotValid string
	DataNotFound string
	IsNotValid   string
}

func InitLanguage(ln string) *Language {
	if ln == "id" {
		return createIDLang()
	}

	return createENLang()
}

func createIDLang() *Language {
	return &Language{
		UUIDNotValid: "UUID tidak valid",
		DataNotFound: "Data tidak ditemukan",
		IsNotValid:   "%s tidak valid",
	}
}

func createENLang() *Language {
	return &Language{
		UUIDNotValid: "UUID not valid",
		DataNotFound: "Data not found",
		IsNotValid:   "%s not valid",
	}
}
