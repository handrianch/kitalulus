package utils

import (
	"github.com/gofiber/fiber/v2"
)

type Response struct {
	status  bool
	code    int
	message string
	data    interface{}
	meta    interface{}
}

func (r *Response) ToJson(c *fiber.Ctx) error {
	return c.Status(r.code).JSON(fiber.Map{
		"status":  r.status,
		"code":    r.code,
		"message": r.message,
		"data":    r.data,
		"meta":    r.meta,
	})
}

func (r *Response) Update(status bool, code int, message string, data, meta interface{}) *Response {
	r.status = status
	r.code = code
	r.message = message
	r.data = data
	r.meta = meta

	return r
}
