package utils

import (
	"strings"

	"github.com/go-playground/validator"
)

type ErrorResponse struct {
	FailedField string
	Tag         string
	Value       string
}

func ValidateStruct(model interface{}) string {
	var errors string

	validate := validator.New()
	err := validate.Struct(model)

	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			return strings.ToLower(err.Field())
		}
	}

	return errors
}
