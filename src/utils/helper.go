package utils

import (
	"strconv"
)

func SetOffset(p, l string) (page, limit, offset int) {
	page, err := strconv.Atoi(p)

	if err != nil {
		page = 1
	}

	limit, err = strconv.Atoi(l)

	if err != nil {
		limit = 10
	}

	offset = (page - 1) * limit

	return
}
