package models

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Question struct {
	ID        uint32    `gorm:"primaryKey;autoIncrement"`
	UUID      uuid.UUID `gorm:"unique;not null;type:string;size:36"`
	Question  string    `gorm:"type:text;not null"`
	IsActive  bool      `gorm:"not null"`
	CreatedBy string    `gorm:"type:string;size:255;not null"`
	UpdatedBy string    `gorm:"type:string;size:255"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (u *Question) BeforeCreate(tx *gorm.DB) error {
	u.UUID = uuid.New()
	return nil
}
