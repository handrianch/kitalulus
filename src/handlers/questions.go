package handlers

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/handrixn/kitalulus-test/src/language"
	"github.com/handrixn/kitalulus-test/src/models"
	"github.com/handrixn/kitalulus-test/src/repositories"
	"github.com/handrixn/kitalulus-test/src/transformers"
	"github.com/handrixn/kitalulus-test/src/utils"
)

var repo = repositories.QuestionRepository{}
var transformer = transformers.QuestionTransformer{}

type QuestionHandlers struct{}

type CreatePayload struct {
	Question  string `json:"question" validate:"required"`
	CreatedBy string `json:"createdBy" validate:"required"`
	IsActive  bool   `json:"isActive" validate:"required"`
}

type UpdatePayload struct {
	Question  string `json:"question" validate:"required"`
	UpdatedBy string `json:"updatedBy" validate:"required"`
	IsActive  bool   `json:"isActive" validate:"required"`
}

func (qh *QuestionHandlers) GetList(c *fiber.Ctx) error {
	page, limit, offset := utils.SetOffset(c.Query("page"), c.Query("limit"))

	questions, count, err := repo.FindAll(nil, offset, limit)

	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	meta := fiber.Map{
		"page":      page,
		"limit":     limit,
		"totalData": count,
	}

	return response.Update(true, http.StatusOK, http.StatusText(http.StatusOK), transformer.List(questions), meta).ToJson(c)
}

func (qh *QuestionHandlers) GetDetail(c *fiber.Ctx) error {
	uuid := c.Params("uuid")
	lang := language.InitLanguage(c.Query("lang"))

	if uuid == "" {
		return fiber.NewError(fiber.StatusBadRequest, lang.UUIDNotValid)
	}

	question, err := repo.FindOne(uuid)

	if err != nil {

		if notFound := strings.Contains(err.Error(), "not found"); notFound {
			return fiber.NewError(fiber.StatusNotFound, lang.DataNotFound)
		}

		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	return response.Update(true, http.StatusOK, http.StatusText(http.StatusOK), transformer.Detail(question), nil).ToJson(c)
}

func (qh *QuestionHandlers) Create(c *fiber.Ctx) error {
	q := new(CreatePayload)
	lang := language.InitLanguage(c.Query("lang"))

	if err := c.BodyParser(q); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	errors := utils.ValidateStruct(q)

	if errors != "" {
		return fiber.NewError(fiber.StatusBadRequest, fmt.Sprintf(lang.IsNotValid, errors))
	}

	p := &models.Question{
		Question:  q.Question,
		IsActive:  q.IsActive,
		CreatedBy: q.CreatedBy,
	}

	question, err := repo.Create(p)

	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	return response.Update(true, http.StatusOK, http.StatusText(http.StatusOK), transformer.Detail(question), nil).ToJson(c)
}

func (qh *QuestionHandlers) Update(c *fiber.Ctx) error {
	uuid := c.Params("uuid")
	lang := language.InitLanguage(c.Query("lang"))

	if uuid == "" {
		return fiber.NewError(fiber.StatusBadRequest, lang.UUIDNotValid)
	}

	question, err := repo.FindOne(uuid)

	if err != nil {
		if notFound := strings.Contains(err.Error(), "not found"); notFound {
			return fiber.NewError(fiber.StatusNotFound, lang.DataNotFound)
		}

		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	q := UpdatePayload{}

	if err := c.BodyParser(&q); err != nil {
		return fiber.NewError(fiber.StatusBadRequest, err.Error())
	}

	errors := utils.ValidateStruct(q)

	if errors != "" {
		return fiber.NewError(fiber.StatusBadRequest, fmt.Sprintf(lang.IsNotValid, errors))
	}

	p := &models.Question{
		Question:  q.Question,
		IsActive:  q.IsActive,
		UpdatedBy: q.UpdatedBy,
		CreatedBy: question.CreatedBy,
	}

	updatedQuestion, err := repo.Update(uuid, p)

	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	return response.Update(true, http.StatusOK, http.StatusText(http.StatusOK), transformer.Detail(updatedQuestion), nil).ToJson(c)
}

func (qh *QuestionHandlers) Delete(c *fiber.Ctx) error {
	uuid := c.Params("uuid")
	lang := language.InitLanguage(c.Query("lang"))

	if uuid == "" {
		return fiber.NewError(fiber.StatusBadRequest, lang.UUIDNotValid)
	}

	_, err := repo.FindOne(uuid)

	if err != nil {
		if notFound := strings.Contains(err.Error(), "not found"); notFound {
			return fiber.NewError(fiber.StatusNotFound, lang.DataNotFound)
		}

		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	err = repo.Delete(uuid)

	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}

	return response.Update(true, http.StatusNoContent, http.StatusText(http.StatusNoContent), nil, nil).ToJson(c)
}
