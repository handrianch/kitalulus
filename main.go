package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/handrixn/kitalulus-test/src/models"
	"github.com/handrixn/kitalulus-test/src/repositories"
	"github.com/handrixn/kitalulus-test/src/routes"
	"github.com/handrixn/kitalulus-test/src/utils"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	app := SetupApp()
	models := models.InitModels()
	repositories.InitRepositories(models.DB)
	routes.InitRouter(app)

	log.Fatal(app.Listen(":3001"))
}

func SetupApp() *fiber.App {
	app := fiber.New(fiber.Config{
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			code := fiber.StatusInternalServerError
			message := http.StatusText(code)

			if e, ok := err.(*fiber.Error); ok {
				code = e.Code
				message = e.Message
			}

			response := utils.Response{}
			return response.Update(false, code, message, nil, nil).ToJson(ctx)
		},
	})

	if _, err := os.Stat(os.Getenv(("LOGGING_FOLDER"))); os.IsNotExist(err) {
		_ = os.MkdirAll(os.Getenv("LOGGING_FOLDER"), os.ModePerm)
	}

	now := time.Now()
	today := now.Format("2006-01-02")

	filename := fmt.Sprintf("%s/%s.log", os.Getenv("LOGGING_FOLDER"), today)

	f, err := os.OpenFile(fmt.Sprintf("./%s", filename), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	defer f.Close()

	app.Use(cors.New())
	app.Use(recover.New())
	app.Use(logger.New(logger.Config{
		Format: "${pid} ${status} - ${method} ${path}\n",
		Output: f,
	}))

	return app
}
