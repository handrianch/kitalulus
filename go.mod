module github.com/handrixn/kitalulus-test

go 1.15

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/gofiber/fiber/v2 v2.2.0
	github.com/google/uuid v1.1.2
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.11.3 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20201126144705-a4b67b81d3d2 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.7
)
